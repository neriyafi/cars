@extends('layouts.app')
@section('content')


<h1> Edit car </h1>
<form method = 'post' action = "{{action('CarController@update', $cars->id)}}"  >
@csrf
@method('PATCH')
<div class = "form-group">

    <label for = "title"> brand: </label>
    <input type = "text" class = "form-control" name = "brand" value = "{{$cars->brand}}">
    <label for = "title"> year:</label>
    <input type = "text" class = "form-control" name = "year" value = "{{$cars->year}}">
    <label for = "title"> price </label>
    <input type = "text" class = "form-control" name = "price" value = "{{$cars->price}}">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Update">
</div>
</form>

<form method = 'post' action = "{{action('CarController@destroy', $cars->id)}}"  >
@csrf
@method('DELETE')
<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Delete">
</div>
</form>

@endsection