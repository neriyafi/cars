<head>

<style>
    h1 {
      color: white;
      text-align: center;
      text-decoration: underline;
    }
          table {
            margin-top: 50px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }

          td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            
          }

          tr:nth-child(even) {
            background-color: #dddddd;
      
          }
          tr:nth-child(odd){
            background-color: #aaaaaa;
      
          }
          tr:nth-child(even):hover {
            background-color: gray;
      
          }
          tr:nth-child(odd):hover {
            background-color: gray;
      
          }

</style>
</head>

@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<body>
<h1>This is the car list</h1>

        <table>
            <tr>
              <th>ID</th>
              <th>Brand</th>
              <th>year</th>
              <th>price</th>
 @can('saller')  <th>user_is</th> @endcan
 @can('saller')   <th>action</th>  @endcan
            </tr>
                    
                    





            @foreach($cars as $car)
            <tr>
            <td>{{$car->id}}</td>
           <td><a href="{{route('cars.edit',$car->id)}}"> {{$car->brand}}</a></td>   
             <td>{{$car->year}}</td>
             <td>{{$car->price}}</td>
  @can('saller')   <td>{{$car->user_id}}</td>
             <td><button id ="{{$car->id}}" value="{{$car->brand}} - on hold"> on hold!</button> </td> @endcan
            </tr>
            @endforeach
     </table>
     
     
     <a href = "{{route('cars.create') }}"> <h1> add a new car </h1> </a>


</body>


<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('cars')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   //data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}), change between 1 and 0
                   data: JSON.stringify({'brand':(event.target.value), _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
   </script>
  
@endsection